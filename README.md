[![downloads](https://img.shields.io/npm/dm/vue2-time-date-axis.svg)](https://www.npmjs.com/package/vue2-time-date-axis)
[![npm-version](https://img.shields.io/npm/v/vue2-time-date-axis.svg)](https://www.npmjs.com/package/vue2-time-date-axis)
[![license](https://img.shields.io/npm/l/express.svg)]()
# Vue2TimeDateAxis

## 🍉 介绍：
欢迎来到`vue2TimeDateAxis`，本项目的开源地址为:[vue2-time-date-axis@码云](https://gitee.com/hawk86104/vue2-time-date-axis) , npm库地址为：[vue2-time-date-axis@npmjs](https://www.npmjs.com/package/vue2-time-date-axis)  


[API文档](http://docs.217dan.com/docs/vue2-time-date-axis/)  

- 本控件是基于 `气象类` 的业务进行梳理，根据需求完善对应的功能，做到尽可能的覆盖气象业务的需求。
不同时间间隔、不同时效的选择，各业务按钮需求函数，EC、GFS等不同模式不同时效、实况和预报的区分，以及默认antd样式，默认插槽，与样式的自由度多插槽的实现。

## 🐳 依赖库包含：
[dayjs](https://dayjs.fenxianglu.cn/) ： 用于时间的格式化以及计算。<br>
[vue-slider-component](https://nightcatsama.github.io/vue-slider-component/) ： 🎚 一个高度定制化的滑块组件。<br>

## 📚 展示：
[Demo地址:](http://docs.217dan.com/docs/vue2-time-date-axis/timeLine-templates.html)


![timeLine截图.png](./imgs/timeLineDemo.png)

## 🚀 快速开始：
```bash
npm install vue2-time-date-axis
# or
yarn add vue2-time-date-axis
```

## 📌 引用：

```vue
<template>
  <timeLine />
</template>

<script>
import { timeLine } from 'vue2-time-date-axis'
import 'vue-slider-component/theme/default.css'

export default {
  components: { timeLine }
}
</script>
```

## ✨ 详细使用：
具体使用细节请查看[帮助文档@.VUEPRESS](http://docs.217dan.com/docs/vue2-time-date-axis/)