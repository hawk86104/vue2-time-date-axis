import Vue from 'vue'
import Dev from './serve.vue'
import Popper from '../src/components/popover'

Vue.config.productionTip = false
Vue.use(Popper)

new Vue({
  render: (h) => h(Dev),
}).$mount('#app')

