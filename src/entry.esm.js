/*
 * @Description: 
 * @Version: 1.668
 * @Autor: Hawk
 * @Date: 2022-05-12 16:32:48
 * @LastEditors: Hawk
 * @LastEditTime: 2022-05-23 16:02:25
 */

import * as components from './com';

export function install(Vue) {
	if (install.installed) return;

	install.installed = true;
	Object.entries(components).forEach(([componentName, component]) => {
		Vue.component(componentName, component);
	});
}
// Create module definition for Vue.use()
const plugin = {
	install
};
// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null;

if (typeof window !== 'undefined') {
	GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
	GlobalVue = global.Vue;
}

if (GlobalVue) {
	GlobalVue.use(plugin);
}

export * from './com';
// const Vue2TimeDateAxis = {
//   install(Vue) {
//     // 注册组件
//     components.forEach(component => {
//       Vue.component(component.name, component);
//     })
//   }
// }

// export default Vue2TimeDateAxis

// Import vue component
// import component from '@/timeLine/index.vue';
// // Default export is installable instance of component.
// // IIFE injects install function into component, allowing component
// // to be registered via Vue.use() as well as Vue.component(),
// export default /*#__PURE__*/(() => {
//   // Get component instance
//   const installable = component;

//   // Attach install function executed by Vue.use()
//   installable.install = (Vue) => {
//     Vue.component('Vue2TimeDateAxis', installable);
//   };
//   return installable;
// })();

// It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = directive;
